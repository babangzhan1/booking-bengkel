package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Car;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class PrintService {
	private static CustomerRepository customerRepository = new CustomerRepository();
	private static ItemServiceRepository itemServiceRepository = new ItemServiceRepository();
	private static Scanner input = new Scanner(System.in);
	private static BengkelService bengkelService = new BengkelService(itemServiceRepository);

	public static void printMenu(String[] listMenu, String title) {
		String line = "+---------------------------------+";
		int number = 1;
		String formatTable = " %-2s. %-25s %n";

		System.out.printf("%-25s %n", title);
		System.out.println(line);

		for (String data : listMenu) {
			if (number < listMenu.length) {
				System.out.printf(formatTable, number, data);
			} else {
				System.out.printf(formatTable, 0, data);
			}
			number++;
		}
		System.out.println(line);
		System.out.println();
	}

	public static void printVechicle(List<Vehicle> listVehicle) {
		String formatTable = "| %-2s | %-15s | %-10s | %-15s | %-15s | %-5s | %-15s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.format(line);
		System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Brand", "Transmisi", "Tahun", "Tipe Kendaraan");
		System.out.format(line);
		int number = 1;
		String vehicleType = "";
		for (Vehicle vehicle : listVehicle) {
			if (vehicle instanceof Car) {
				vehicleType = "Mobil";
			} else {
				vehicleType = "Motor";
			}
			System.out.format(formatTable, number, vehicle.getVehiclesId(), vehicle.getColor(), vehicle.getBrand(),
					vehicle.getTransmisionType(), vehicle.getYearRelease(), vehicleType);
			number++;
		}
		System.out.printf(line);
	}

	// Silahkan Tambahkan function print sesuai dengan kebutuhan.
	public static String loginCustomer(List<Customer> custList) {
		System.out.println("=== Login Customer ===");
		System.out.print("Masukkan Customer Id: ");
		String custId = input.nextLine();
		System.out.print("Masukkan Password: ");
		String password = input.nextLine();

		try {
			String loginMessage = bengkelService.login(custId, password);
			System.out.println(loginMessage);
			return loginMessage;
		} catch (Exception e) {
			System.out.println("Terjadi kesalahan saat melakukan login: " + e.getMessage());
			return "Gagal melakukan login.";
		}
	}

	public static void printServiceItems(List<ItemService> availableItemServices) {
		String formatTable = "| %-2s | %-15s | %-30s | %-15s |%n";
		String line = "+----+-----------------+--------------------------------+-----------------+%n";
		System.out.format(line);
		System.out.format(formatTable, "No", "Service Id", "Nama Service", "Harga");
		System.out.format(line);
		int number = 1;
		for (ItemService item : availableItemServices) {
			System.out.format(formatTable, number, item.getServiceId(), item.getServiceName(), item.getPrice());
			number++;
		}
		System.out.printf(line);
	}

	public static void informasiCustomer(Customer loggedInCust) {
		System.out.println("=== Informasi Customer ===");
		System.out.println("\tCustomer Profile\n");
		System.out.println("\tCustomer Profile\n");
		System.out.printf("Customer Id: %13s\n", loggedInCust.getCustomerId());
		System.out.printf("Nama:        %10s\n", loggedInCust.getName());
		System.out.printf("Customer Status: %s\n", (loggedInCust instanceof MemberCustomer) ? "Member" : "Non-Member");
		System.out.printf("Alamat:      %12s\n", loggedInCust.getAddress());
		if (loggedInCust instanceof MemberCustomer){
			System.out.printf("Saldo Coin:   %12s\n", ((MemberCustomer) loggedInCust).getSaldoCoin());
		}
		System.out.println("\tList Kendaraan: ");
		System.out.println("\tNo\tVehicle Id\tWarna\tTipe Kendaraan\tTahun");

		int vehicleNo = 1;
		for (Vehicle vehicle : loggedInCust.getVehicles()) {
			System.out.printf("\t%d\t%-12s\t%-6s\t%-15s\t%d\n",
					vehicleNo++,
					vehicle.getVehiclesId(),
					vehicle.getColor(),
					vehicle.getVehicleType(),
					vehicle.getYearRelease());
		}
		System.out
				.println("+========================================================================================+");
	}

	public static void bookingBengkel(Customer loggedInCust) {
		System.out.println("=== Booking Bengkel ===");
		if (loggedInCust == null) {
			System.out.println("Harap login terlebih dahulu untuk melakukan booking service.");
			return;
		}

		PrintService.printVechicle(loggedInCust.getVehicles());

		Scanner scanner = new Scanner(System.in);

		String vehicleId = Validation.validasiInput("Masukkan Vehicle Id yang ingin diberi layanan: ",
				"Input tidak valid!", "\\w+");
		Optional<Vehicle> optionalVehicle = customerRepository.findVehicleById(vehicleId);
		if (!optionalVehicle.isPresent()) {
			System.out.println("Kendaraan dengan Vehicle Id " + vehicleId + " tidak ditemukan.");
			return;
		}

		List<ItemService> availableServices = itemServiceRepository.findServiceItemsByVehicleType(optionalVehicle.get().getVehicleType());
		PrintService.printServiceItems(availableServices);

		List<String> serviceItemIds = new ArrayList<>();
		while (true){
			String serviceItemId = Validation.validasiInput("Masukkan Service Item Id yang dipilih: ", "Input tidak valid!",
				"[\\w-]+");
			Optional<ItemService> optionalItemService = availableServices.stream()
			.filter(item -> item.getServiceId().equalsIgnoreCase(serviceItemId))
			.findFirst();
			if (!optionalItemService.isPresent()) {
				System.out.println("Service Item dengan Id " + serviceItemId + " tidak ditemukan untuk kendaraan ini.");
				continue;
		}
		serviceItemIds.add(serviceItemId);
		System.out.print("Apakah anda ingin menambahkan Service Lainnya? (Y/T): ");
        String addMore = scanner.nextLine().trim();
        if (!addMore.equalsIgnoreCase("Y")) {
            break;
        }
	}

	String paymentMethod = "";
	while (true){
		paymentMethod = Validation.validasiInput("Masukkan Metode Pembayaran (coin/cash): ",
				"Metode pembayaran tidak valid!", "(?i)coin|cash");
		if (paymentMethod.equalsIgnoreCase("coin") || paymentMethod.equalsIgnoreCase("cash")) {
		break;
		}
	}
		String bookingMessage = bengkelService.bookingService(loggedInCust,vehicleId, serviceItemIds, paymentMethod);
		System.out.println("\n" + bookingMessage);
	}

	public static void topUpBengkelCoin(Customer loggedInCust ) {
		System.out.println("=== Top Up Bengkel Coin ===");
		if (loggedInCust == null) {
			System.out.println("Harap login terlebih dahulu untuk bisa melakukan top up.");
			return;
		}

		String regex = "\\d+";
		double amount = Validation.validasiNumberWithRange("Masukkan jumlah top-up: ", "Input tidak valid.", regex,
				Integer.MAX_VALUE, 1);

		boolean isSuccess = bengkelService.topUpSaldoCoin(loggedInCust, amount);

		if (isSuccess) {
			System.out.println("Top-up berhasil.");
		} else {
			System.out.println("Top-up gagal.");
		}
	}

	public static void informasiBooking(Customer loggedInCust) {
		System.out.println("=== Informasi Booking Order ===");

		if (loggedInCust == null) {
			System.out.println("Harap login terlebih dahulu untuk melihat informasi booking.");
			return;
		}

		List<BookingOrder> bookingOrders = bengkelService
				.getBookingOrdersForCustomer(loggedInCust);

		if (bookingOrders.isEmpty()) {
			System.out.println("Tidak ada booking order untuk customer ini.");
			return;
		}

		PrintService.printBookingOrders(bookingOrders);
	}

	public static void printBookingOrders(List<BookingOrder> bookingOrders) {
		String formatTable = "| %-2s | %-17s | %-15s | %-15s | %-13s | %-13s | %-25s |%n";
		String line = "+----+-------------------+-----------------+-----------------+---------------+---------------+---------------------------+%n";
		System.out.format(line);
		System.out.format(formatTable, "No", "Booking Id", "Nama Customer", "Payment Method", "Total Service",
				"Total Payment", "List Service");
		System.out.format(line);
		int number = 1;
		for (BookingOrder order : bookingOrders) {
			StringBuilder listServices = new StringBuilder();
			for (ItemService service : order.getServices()) {
				listServices.append(service.getServiceName()).append(", ");
			}
			listServices.delete(listServices.length() - 2, listServices.length()); // Remove trailing comma and space

			System.out.format(formatTable,
					number,
					order.getBookingId(),
					order.getCustomer().getName(),
					order.getPaymentMethod(),
					order.getTotalServicePrice(),
					order.getTotalPayment(),
					listServices.toString());
			number++;
		}
		System.out.printf(line);
	}

}
