package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class BengkelService {
	private CustomerRepository customerRepository;
	private Customer loggedInCust;
	private ItemServiceRepository itemServiceRepository;
	private List<BookingOrder> bookingOrders;

	public BengkelService(ItemServiceRepository itemServiceRepository, CustomerRepository customerRepository) {
		this.itemServiceRepository = itemServiceRepository;
		this.customerRepository = customerRepository;
		this.bookingOrders = new ArrayList<>();
	}

	public BengkelService() {
		this.customerRepository = new CustomerRepository();
		this.itemServiceRepository = new ItemServiceRepository();
		this.bookingOrders = new ArrayList<>();
	}

	public BengkelService(ItemServiceRepository itemServiceRepository) {
		this.itemServiceRepository = itemServiceRepository;
		this.customerRepository = new CustomerRepository();
	}

	public Customer getLoggedInCust() {
		return loggedInCust;
	}

	public void setLoggedInCust(Customer loggedInCust) {
		this.loggedInCust = loggedInCust;
	}

	// Silahkan tambahkan fitur-fitur utama aplikasi disini

	// Login
	public String login(String custId, String password) {
		Optional<Customer> optionalCust = customerRepository.findCustById(custId);
		if (optionalCust.isPresent()) {
			Customer customer = optionalCust.get();
			if (customer.getPassword().equals(password)) {
				loggedInCust = customer;
				return "Login berhasil! Selamat datang, " + customer.getName() + "!";
			} else {
				return "Password yang anda masukkan salah!";
			}
		} else {
			return "Customer Id tidak ditemukan atau salah!";
		}

	}

	// Info Customer
	public String getInfoCustLogin(Customer loggedInCust) {
		if (loggedInCust != null) {
			StringBuilder info = new StringBuilder();
			info.append("Informasi Customer:\n");
			info.append("Customer Id: ").append(loggedInCust.getCustomerId()).append("\n");
			info.append("Nama: ").append(loggedInCust.getName()).append("\n");
			info.append("Alamat: ").append(loggedInCust.getAddress()).append("\n");

			if (loggedInCust instanceof MemberCustomer) {
				info.append("Status: Member\n");
				MemberCustomer memberCustomer = (MemberCustomer) loggedInCust;
				info.append("Saldo Member: ").append(memberCustomer.getSaldoCoin()).append("\n");
			} else {
				info.append("Status: Non-Member\n");
			}

			info.append("List Kendaraan:\n");
			int vehicleNo = 1;
			for (Vehicle vehicle : loggedInCust.getVehicles()) {
				info.append("No: ").append(vehicleNo++).append("\n");
				info.append("Vehicle Id: ").append(vehicle.getVehiclesId()).append("\n");
				info.append("Warna: ").append(vehicle.getColor()).append("\n");
				info.append("Type Kendaraan: ").append(vehicle.getVehicleType()).append("\n");
				info.append("Tahun: ").append(vehicle.getYearRelease()).append("\n");

			}
			return info.toString();
		} else {
			return "Tidak ada data customer.";
		}
	}

	// Booking atau Reservation
	public String bookingService(Customer loggedInCust, String vehicleId, List<String> serviceItemIds,
			String paymentMethod) {
		if (loggedInCust == null) {
			return "Harap login terlebih dahulu untuk melakukan booking service.";
		}

		Optional<Vehicle> optionalVehicle = customerRepository.findVehicleById(vehicleId);
		if (!optionalVehicle.isPresent()) {
			return "Kendaraan dengan Vehicle Id " + vehicleId + " Tidak ditemukan.";
		}

		Vehicle vehicle = optionalVehicle.get();

		List<ItemService> availableItemServices = itemServiceRepository
				.findServiceItemsByVehicleType(vehicle.getVehicleType());

		List<ItemService> selectedServices = new ArrayList<>();

		for (String serviceItemId : serviceItemIds) {
			Optional<ItemService> optionalItemService = availableItemServices.stream()
					.filter(item -> item.getServiceId().equals(serviceItemId))
					.findFirst();

			if (!optionalItemService.isPresent()) {
				return "Service Item dengan ID " + serviceItemId + " Tidak ditemukan untuk kendaraan ini.";
			}

			ItemService selectedItemService = optionalItemService.get();
			selectedServices.add(selectedItemService);
		}

		double totalPayment = selectedServices.stream()
				.mapToDouble(ItemService::getPrice)
				.sum();

		boolean isMember = loggedInCust instanceof MemberCustomer;
		boolean isCoinPayment = paymentMethod.equalsIgnoreCase("coin");

		if (isMember && isCoinPayment) {
			if (totalPayment > ((MemberCustomer) loggedInCust).getSaldoCoin()) {
				return "Saldo Coin tidak mencukupi untuk pembayaran.";
			}
			((MemberCustomer) loggedInCust).setSaldoCoin(((MemberCustomer) loggedInCust).getSaldoCoin() - totalPayment);
			totalPayment *= 0.9;
		} else if (!isMember && isCoinPayment) {
			return "Hanya Member yang dapat menggunakan pembayaran dengan coin.";
		}

		String custId = loggedInCust.getCustomerId();
		int nextBookingNumber = bookingOrders.size() + 1;
		String bookiggId = "Book-" + custId + "-" + String.format("%03d", nextBookingNumber);

		BookingOrder bookingOrder = new BookingOrder();
		bookingOrder.setBookingId(bookiggId);
		bookingOrder.setCustomer(loggedInCust);
		bookingOrder.setServices(selectedServices);
		bookingOrder.setPaymentMethod(paymentMethod);
		bookingOrder.setTotalServicePrice(totalPayment);
		bookingOrder.setTotalPayment(totalPayment);

		if (bookingOrders == null) {
			bookingOrders = new ArrayList<>();
		}

		bookingOrders.add(bookingOrder);

		String bookingMessage = "Booking Service Berhasil:\n";
		bookingMessage += "Customer: " + loggedInCust.getName() + "\n";
		bookingMessage += "Kendaraan: " + vehicle.getVehiclesId() + " - " + vehicle.getVehicleType() + "\n";
		bookingMessage += "Service Items:\n";
		for (ItemService service : selectedServices) {
			bookingMessage += "  - " + service.getServiceName() + "\n";
		}
		bookingMessage += "Total Pembayaran: " + totalPayment + " IDR\n";
		bookingMessage += "Metode Pembayaran: " + paymentMethod + "\n";
		return bookingMessage;
	}

	public List<BookingOrder> getBookingOrdersForCustomer(Customer loggedInCust) {
		List<BookingOrder> ordersForCustomer = new ArrayList<>();
		if (loggedInCust == null) {
			return ordersForCustomer;
		}

		for (BookingOrder order : bookingOrders) {
			Customer orderCustomer = order.getCustomer();
			if (orderCustomer != null && orderCustomer.getCustomerId().equals(loggedInCust.getCustomerId())) {
				ordersForCustomer.add(order);
			}
		}
		return ordersForCustomer;
	}

	// Top Up Saldo Coin Untuk Member Customer
	public boolean topUpSaldoCoin(Customer loggedInCust, double amount) {
		if (amount <= 0) {
			System.out.println("Jumlah top-up harus lebih dari 0.");
			return false;
		}

		Optional<Customer> optionalCust = customerRepository.findCustById(loggedInCust.getCustomerId());
		if (optionalCust.isPresent() && optionalCust.get() instanceof MemberCustomer) {
			MemberCustomer memberCustomer = (MemberCustomer) optionalCust.get();
			double currentSaldoCoin = memberCustomer.getSaldoCoin();
			double newSaldoCoin = currentSaldoCoin + amount;
			memberCustomer.setSaldoCoin(newSaldoCoin);

			customerRepository.updateMemberCustomer(memberCustomer);

			if (loggedInCust.getCustomerId().equals(memberCustomer.getCustomerId())) {
				loggedInCust = memberCustomer;
			}

			System.out.println("Top-up berhasil. Saldo coin saat ini: " + memberCustomer.getSaldoCoin());
			return true;
		} else {
			System.out.println("Customer tidak ditemukan atau bukan member.");
			return false;
		}
	}

	public void logout() {
		loggedInCust = null;
		System.out.println("Logout berhasil.");
	}

}
