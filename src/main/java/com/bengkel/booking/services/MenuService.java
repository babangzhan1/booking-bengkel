package com.bengkel.booking.services;

import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class MenuService {
	// private static List<Customer> listAllCustomers =
	// CustomerRepository.getAllCustomer();
	// private static List<ItemService> listAllItemServices =
	// ItemServiceRepository.getAllItemService();
	// private static ItemServiceRepository listAllItemService = new
	// ItemServiceRepository();
	// private static BengkelService bengkelService = new
	// BengkelService(listAllItemService);
	// private static Scanner input = new Scanner(System.in);
	private static List<Customer> listAllCustomers;
	private static List<ItemService> listAllItemServices;
	private static CustomerRepository customerRepository;
	private static ItemServiceRepository itemServiceRepository;
	private static BengkelService bengkelService;
	private static Scanner input;

	static {
		customerRepository = new CustomerRepository();
		itemServiceRepository = new ItemServiceRepository();
		listAllCustomers = customerRepository.getAllCustomer();
		listAllItemServices = itemServiceRepository.getAllItemService();
		bengkelService = new BengkelService(itemServiceRepository);
		input = new Scanner(System.in);
	}

	public static void run() {
		boolean isLooping = true;
		do {
			printStartMenu();
			int choice = Validation.validasiNumberWithRange("Masukan Pilihan Menu: ", "Input Harus Berupa Angka!",
					"^[0-9]+$", 1, 0);
			switch (choice) {
				case 1:
					boolean loginSuccess = login();
					if (loginSuccess) {
						mainMenu();
					} else {
						isLooping = false;
					}
					break;
				case 0:
					System.out.println("Terima kasih telah menggunakan aplikasi Booking Bengkel.");
					isLooping = false;
					break;
				default:
					System.out.println("Pilihan tidak valid. Silakan masukkan pilihan yang sesuai.");
					break;
			}
		} while (isLooping);

	}

	public static boolean login() {
		System.out.println("=== Login ===");

		String custId;
		String password;
		boolean isValid = false;

		do {
			custId = Validation.validasiInput("Masukkan Customer Id: ",
					"Customer Id tidak valid! Harap masukkan kembali.", "^Cust-[\\w\\d]+$");
			password = Validation.validasiInput("Masukkan Password: ", "Password tidak valid!", ".+");
			if (custId != null && password != null) {
				String loginMessage = bengkelService.login(custId, password);
				System.out.println(loginMessage);
				loginMessage.contains("Login berhasil");
				return true;
			} else {
				System.out.println("Login gagal. Silakan coba lagi.");
			}
		} while (!isValid);
		return false;
	}

	public static void mainMenu() {
		String[] listMenu = { "Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking",
				"Logout" };
		int menuChoice = 0;
		boolean isLooping = true;

		do {
			PrintService.printMenu(listMenu, "Booking Bengkel Menu");
			menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!",
					"^[0-9]+$", listMenu.length - 1, 0);
			System.out.println(menuChoice);

			switch (menuChoice) {
				case 1:
					// panggil fitur Informasi Customer
					if (bengkelService.getLoggedInCust() != null) {
						PrintService.informasiCustomer(bengkelService.getLoggedInCust());
					} else {
						System.out.println("Anda belum login.");
					}
					break;
				case 2:
					// panggil fitur Booking Bengkel
					if (bengkelService.getLoggedInCust() != null){
						PrintService.bookingBengkel(bengkelService.getLoggedInCust());
					} else {
						System.out.println("Anda belum login.");
					}				
					break;
				case 3:
					// panggil fitur Top Up Saldo Coin
					if (bengkelService.getLoggedInCust() != null){
						PrintService.topUpBengkelCoin(bengkelService.getLoggedInCust());
					} else {
						System.out.println("Anda belum login.");
					}				
					break;
				case 4:
					// panggil fitur Informasi Booking Order
					if (bengkelService.getLoggedInCust() != null){
						PrintService.informasiBooking(bengkelService.getLoggedInCust());
					} else {
						System.out.println("Anda belum login.");
					}		
					break;
				default:
					System.out.println("Logout");
					isLooping = false;
					break;
			}
		} while (isLooping);

	}

	// Silahkan tambahkan kodingan untuk keperluan Menu Aplikasi
	private static void printStartMenu() {
		System.out.println("Menu Awal\n");
		System.out.println("Aplikasi Booking Bengkel\n");
		System.out.println("1. Login");
		System.out.println("0. Exit\n");
	}

	private static void printHomeMenu() {
		System.out.println("Menu Home\n");
		System.out.println("Selamat Datang di Aplikasi Booking Bengkel\n");
		System.out.println("1. Informasi Customer");
		System.out.println("2. Booking");
		System.out.println("3. Top Up Saldo Coin");
		System.out.println("4. Informasi Booking Order");
		System.out.println("0. Logout\n");
	}
}
